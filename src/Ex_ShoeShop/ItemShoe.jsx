import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    return (
      <div className="col-3 py-2">
        <div className="card text-left h-100">
          <img
            className="card-img-top"
            src={this.props.data.image}
            alt="true"
          />
          <div className="card-body">
            <h4 className="card-title">{this.props.data.name}</h4>
            <button
              className="btn btn-primary mr-2"
              onClick={() => {
                this.props.handleAddToCart(this.props.data);
              }}
            >
              Mua
            </button>
            <button
              className="btn btn-secondary"
              onClick={() => {
                this.props.handleChangeDetail(this.props.data);
              }}
            >
              Xem chi tiết
            </button>
          </div>
        </div>
      </div>
    );
  }
}
